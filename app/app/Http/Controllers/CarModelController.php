<?php

namespace App\Http\Controllers;

use App\Models\CarModel;
use App\Http\Resources\CarModelCollection;

class CarModelController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/models",
     *      summary="Список моделей автомобилей",
     *      description="Возвращает список моделей автомобилей",
     *      operationId="modelsList",
     *      tags={"Модели автомобилей"},
     *      @OA\Response(
     *          response=200,
     *          description="Запрос выполнен успешно",
     *      )
     * )
     *
     * @return mixed
     */
    public function index()
    {
        return CarModelCollection::collection(CarModel::all());
    }
}
