<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('car_models', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("brand_id");
            $table->string("name");
            $table->timestamps();

            $table->foreign('brand_id')->references('id')->on('brands');
        });

        DB::table("car_models")->insert([
            'brand_id' => 1,
            'name' => 'X5'
        ]);

        DB::table("car_models")->insert([
            'brand_id' => 1,
            'name' => 'X3'
        ]);

        DB::table("car_models")->insert([
            'brand_id' => 2,
            'name' => 'GLC'
        ]);

        DB::table("car_models")->insert([
            'brand_id' => 3,
            'name' => 'RAV4'
        ]);

        DB::table("car_models")->insert([
            'brand_id' => 3,
            'name' => 'Camry'
        ]);

        DB::table("car_models")->insert([
            'brand_id' => 4,
            'name' => '2115'
        ]);

        DB::table("car_models")->insert([
            'brand_id' => 4,
            'name' => '2113'
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('car_models');
    }
};
