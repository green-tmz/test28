<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CarResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'release_year' => $this->release_year,
            'color' => $this->color,
            'mileage' => $this->mileage,
            'brand' => $this->brand->name,
            'model' => $this->model->name
        ];
    }
}
