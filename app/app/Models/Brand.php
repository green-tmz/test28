<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class Brand extends Model
{
    use HasFactory;

    public function models(): BelongsTo
    {
        return $this->belongsTo(CarModel::class, 'brand_id', 'id');
    }
}
