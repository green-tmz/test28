<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CarController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\BrandController;
use App\Http\Controllers\UserCarController;
use App\Http\Controllers\CarModelController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::controller(AuthController::class)->group(function () {
    Route::post('login', 'login');
    Route::post('register', 'register');
});

Route::middleware(['auth:sanctum'])->group(function () {
    Route::post('/binding/{car}', [UserCarController::class, 'bindingUserCars']);
    Route::post('/binding/{car}/untie', [UserCarController::class, 'untieUserCars']);
    Route::get('/binding/list', [UserCarController::class, 'bindingList']);
});

Route::get('brands', [BrandController::class, 'index']);
Route::get('models', [CarModelController::class, 'index']);
Route::resource('cars', CarController::class);
