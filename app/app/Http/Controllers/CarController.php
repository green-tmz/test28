<?php

namespace App\Http\Controllers;

use App\Models\Car;
use App\Http\Requests\CarRequest;
use App\Http\Resources\CarCollection;
use App\Http\Resources\CarResource;

class CarController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/cars",
     *      summary="Список автомобилей",
     *      summary="Возвращает список автомобилей",
     *      description="Возвращает список автомобилей",
     *      operationId="carsList",
     *      tags={"Автомобили"},
     *      @OA\Response(
     *          response=200,
     *          description="Запрос выполнен успешно",
     *      )
     * )
     *
     * @return mixed
     */
    public function index()
    {
        return CarCollection::collection(Car::all());
    }

    /**
     * Создание автомобиля.
     *
     * @OA\Post(
     *     path="/api/cars",
     *     operationId="carsStore",
     *     tags={"Автомобили"},
     *     summary="Создание автомобиля",
     *     description="Создание автомобиля",
     *     @OA\Parameter(
     *         description="id марки",
     *         in="query",
     *         name="brand_id",
     *         required=true,
     *         example="1"
     *     ),
     *     @OA\Parameter(
     *         description="id модели",
     *         in="query",
     *         name="model_id",
     *         required=true,
     *         example="2"
     *     ),
     *     @OA\Parameter(
     *         description="Цвет",
     *         in="query",
     *         name="color",
     *         example="#cccccc"
     *     ),
     *     @OA\Parameter(
     *         description="Год выпуска",
     *         in="query",
     *         name="release_year",
     *         example="2023"
     *     ),
     *     @OA\Parameter(
     *         description="Пробег",
     *         in="query",
     *         name="mileage",
     *         example="10000"
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\JsonContent(
     *             @OA\Property(property="id", type="integer", example="1"),
     *             @OA\Property(property="name", type="string", example="Название категории"),
     *         )
     *     )
     * )
     *
     */
    public function store(CarRequest $request)
    {
        $car = Car::create($request->validated());
        return new CarResource($car);
    }

    /**
     * Информация об автомобиле.
     *
     * @OA\Get(
     *     path="/api/cars/{car}",
     *     operationId="carsEdit",
     *     tags={"Автомобили"},
     *     summary="Информация об автомобиле",
     *     description="Информация об автомобиле",
     *     @OA\Parameter(
     *         description="id автомобиля",
     *         in="path",
     *         name="car",
     *         required=true,
     *         example="1"
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Данные для редактирование категории",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="data",
     *                 type="array",
     *                 @OA\Items(
     *                     @OA\Property(property="name", type="string", example="Название категории"),
     *                     @OA\Property(property="code", type="string", example="name"),
     *                     @OA\Property(property="require", type="boolean", example="true"),
     *                     @OA\Property(property="value", type="string", example="Название категории"),
     *                 )
     *             )
     *         )
     *     )
     * )
     *
     */
    public function show(Car $car)
    {
        return new CarResource($car);
    }

    /**
     * Обновление автомобиля.
     *
     * @OA\Put(
     *     path="/api/cars/{car}",
     *     operationId="categoryUpdate",
     *     tags={"Автомобили"},
     *     summary="Обновление автомобиля",
     *     description="Обновление автомобиля",
     *     @OA\Parameter(
     *         description="id автомобиля",
     *         in="path",
     *         name="car",
     *         required=true,
     *         example="1"
     *     ),
     *     @OA\Parameter(
     *         description="id марки",
     *         in="query",
     *         name="brand_id",
     *         required=true,
     *         example="1"
     *     ),
     *     @OA\Parameter(
     *         description="id модели",
     *         in="query",
     *         name="model_id",
     *         required=true,
     *         example="2"
     *     ),
     *     @OA\Parameter(
     *         description="Цвет",
     *         in="query",
     *         name="color",
     *         example="#cccccc"
     *     ),
     *     @OA\Parameter(
     *         description="Год выпуска",
     *         in="query",
     *         name="release_year",
     *         example="2023"
     *     ),
     *     @OA\Parameter(
     *         description="Пробег",
     *         in="query",
     *         name="mileage",
     *         example="10000"
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\JsonContent(
     *             @OA\Property(property="id", type="integer", example="1"),
     *             @OA\Property(property="name", type="string", example="Новое название"),
     *         )
     *     )
     * )
     *
     */
    public function update(CarRequest $request, Car $car)
    {
        $car->update($request->validated());
        return new CarResource($car);
    }

    /**
     * Удаление автомобиля.
     *
     * @OA\Delete(
     *     path="/api/cars/{car}",
     *     operationId="carsDestroy",
     *     tags={"Автомобили"},
     *     summary="Удаление автомобиля",
     *     description="Удаление автомобиля",
     *     @OA\Parameter(
     *         description="id автомобиля",
     *         in="path",
     *         name="car",
     *         required=true,
     *         example="1"
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Запрос выполнен успешно",
     *     )
     * )
     *
     */
    public function destroy(Car $car)
    {
        if ($car->user_id != NULL) {
            return response()->json([
                'error' => [
                    'error_code' => 2,
                    'error_msg' => 'Ошибка удаления. Автомобиль привязан к пользователю',
                ],
            ], 401);
        }
        $car->delete();
        return $this->index();
    }
}
