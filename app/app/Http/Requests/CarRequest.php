<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'color' => ['string', 'regex:/^#([0-9a-fA-F]{3}|[0-9a-fA-F]{6}|[0-9a-fA-F]{8})$/'],
            'release_year' => ['integer', 'regex:/^[0-9]{4}$/'],
            'mileage' => ['integer'],
            'brand_id' => ['integer', 'exists:App\Models\Brand,id'],
            'model_id' => ['integer', 'exists:App\Models\CarModel,id'],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'color' => 'Не верно указан цвет',
            'release_year.regex' => 'Не верно указан год',
            'release_year.integer' => 'Год должен быть числом',
            'mileage.integer' => 'Пробег должен быть числом',
            'brand_id.exists' => 'Марка с таким id не существует',
            'brand_id.integer' => 'Марка должна быть числом',
            'model_id.exists' => 'Модель с таким id не существует',
            'model_id.integer' => 'Модель должна быть числом',
        ];
    }
}
