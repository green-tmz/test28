<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Http\Resources\BrandCollection;

class BrandController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/brands",
     *      summary="Список марок автомобилей",
     *      description="Возвращает список марок автомобилей",
     *      operationId="brandsList",
     *      tags={"Марки автомобилей"},
     *      @OA\Response(
     *          response=200,
     *          description="Запрос выполнен успешно",
     *      )
     * )
     *
     * @return mixed
     */
    public function index()
    {
        return BrandCollection::collection(Brand::all());
    }
}
