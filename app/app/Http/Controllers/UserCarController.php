<?php

namespace App\Http\Controllers;

use App\Models\Car;
use Illuminate\Http\Request;
use App\Http\Resources\CarCollection;
use App\Http\Resources\UserCarResource;
use Laravel\Sanctum\PersonalAccessToken;

class UserCarController extends Controller
{
    /**
     * Привязка пользователя к автомобилю.
     *
     * @OA\Post(
     *     path="/api/binding/{car}",
     *     operationId="bindingUserCars",
     *     tags={"Привязка"},
     *     summary="Привязка пользователя к автомобилю",
     *     description="Привязка пользователя к автомобилю",
     *     security={ {"sanctum": {} }},
     *     @OA\Parameter(
     *         description="id автомобиля",
     *         in="path",
     *         name="car",
     *         required=true,
     *         example="1"
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\JsonContent(
     *             @OA\Property(property="id", type="integer", example="1"),
     *             @OA\Property(property="name", type="string", example="Название тега"),
     *             @OA\Property(property="color", type="string", example="Цвет тега"),
     *         )
     *     )
     * )
     *
     */
    public function bindingUserCars(Request $request, Car $car)
    {
        $authData = explode(" ", $request->header('authorization'));

        $token = PersonalAccessToken::findToken($authData[1]);
        $user = $token->tokenable;

        if ($car->user_id != NULL) {
            return response()->json([
                'error' => [
                    'error_code' => 2,
                    'error_msg' => 'Этот автомобиль уже привязан',
                ],
            ], 401);
        }

        $car->update([
            'user_id' => $user->id
        ]);
        return new UserCarResource($car);
    }

    /**
     * Список привязанных автомобилей.
     *
     * @OA\Get(
     *     path="/api/binding/list",
     *     operationId="bindingList",
     *     tags={"Привязка"},
     *     summary="Список привязанных автомобилей",
     *     description="Список привязанных автомобилей",
     *     security={ {"sanctum": {} }},
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\JsonContent(
     *             @OA\Property(property="id", type="integer", example="1"),
     *             @OA\Property(property="name", type="string", example="Название тега"),
     *             @OA\Property(property="color", type="string", example="Цвет тега"),
     *         )
     *     )
     * )
     *
     */
    public function bindingList(Request $request)
    {
        $authData = explode(" ", $request->header('authorization'));

        $token = PersonalAccessToken::findToken($authData[1]);
        $user = $token->tokenable;

        $cars = Car::whereUserId($user->id)->get();
        return CarCollection::collection($cars);
    }

    /**
     * Отвязать пользователя от автомобиля.
     *
     * @OA\Post(
     *     path="/api/binding/{car}/untie",
     *     operationId="untieUserCars",
     *     tags={"Привязка"},
     *     summary="Отвязать пользователя от автомобиля",
     *     description="Отвязать пользователя от автомобиля",
     *     security={ {"sanctum": {} }},
     *     @OA\Parameter(
     *         description="id автомобиля",
     *         in="path",
     *         name="car",
     *         required=true,
     *         example="1"
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="",
     *         @OA\JsonContent(
     *             @OA\Property(property="id", type="integer", example="1"),
     *             @OA\Property(property="name", type="string", example="Название тега"),
     *             @OA\Property(property="color", type="string", example="Цвет тега"),
     *         )
     *     )
     * )
     *
     */
    public function untieUserCars(Request $request, Car $car)
    {
        $authData = explode(" ", $request->header('authorization'));

        $token = PersonalAccessToken::findToken($authData[1]);
        $user = $token->tokenable;

        if ($car->user_id == $user->id) {
            $car->update([
                'user_id' => NULL
            ]);
        }

        $cars = Car::whereUserId($user->id)->get();
        return CarCollection::collection($cars);
    }
}
