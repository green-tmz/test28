<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CarCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @return array<int|string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'release_year' => $this->release_year,
            'mileage' => $this->mileage,
            'color' => $this->color,
            'brand' => $this->brand->name,
            'model' => $this->model->name,
        ];
    }
}
